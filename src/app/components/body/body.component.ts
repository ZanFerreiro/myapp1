import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  mostrar = true;
  frase:any = {
    autor:'Ben Parker',
    mensaje:'Un gran poder conlleva una gran responsabilidad',
  }
  
  personajes:string[] = ['Spider Man','Super man','Batman']
  constructor() { }

  ngOnInit() {
  }

}
